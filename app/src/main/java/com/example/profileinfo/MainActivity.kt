package com.example.profileinfo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        // This code indicates that on the click of the saveButton saveInfo() function activates.
        saveButton.setOnClickListener {
            saveInfo()
        }

        // On the long click of the clearButton information is cleared from all fields.
        clearButton.setOnLongClickListener {
            emailEdT.setText("")
            usernameEdT.setText("")
            firstNameEdT.setText("")
            lastNameEdT.setText("")
            ageEdT.setText("")
            true
        }

    }

    // This function checks whether the email is in valid format or not and sets an error message
    // on emailEdT field.
    private fun emailChecker() {
        val email = emailEdT.text.toString()
        if (email.isNotEmpty()) {
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            else emailEdT.error = "Email format is not correct"
        }
    }

    // This function checks the length of the username. If it's less than 10, error message is put
    // In according field
    private fun userNameChecker() {
        val username = usernameEdT.text.toString()
        if (username.isNotEmpty() && username.length < 10) {
            usernameEdT.error = "You must use 10 or more characters"
        }
    }

    // This function checks the age field. If age is negative or non-Integer value error message
    // Is put in the age field.
    private fun ageChecker() {
        val age = ageEdT.text.toString()
        if (age.isNotEmpty() && age.toInt() < 0 && age.contains('.')) {
            ageEdT.error = "Please enter valid age"
        }
    }

    // emailChecker, userNameChecker and ageChecker functions are called in saveInfo function.
    // This function is also responsible of checking whether all fields are filled or not.
    private fun saveInfo() {
        emailChecker()
        userNameChecker()
        ageChecker()

        val email = emailEdT.text.toString()
        val username = usernameEdT.text.toString()
        val firstName = firstNameEdT.text.toString()
        val lastName = lastNameEdT.text.toString()
        val age = ageEdT.text.toString()

        if (email.isEmpty() || username.isEmpty() || firstName.isEmpty() ||
            lastName.isEmpty() || age.isEmpty()
        ) {
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }

    }
}